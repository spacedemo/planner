package org.spacedemo.planner.repository;

import java.time.Instant;
import java.util.List;

import org.spacedemo.planner.model.Acquisition;
import org.springframework.data.repository.CrudRepository;

public interface AcquisitionRepository extends CrudRepository<Acquisition, Instant> {

    public List<Acquisition> findByDateAfter(Instant date);


    public Acquisition findById(String id);


    public Acquisition save(Acquisition Acquisition);

}
