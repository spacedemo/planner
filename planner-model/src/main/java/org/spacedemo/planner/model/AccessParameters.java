package org.spacedemo.planner.model;


public class AccessParameters {

	private  Interval interval;

    private Position position;

	public Interval getInterval() {
		return interval;
	}

	public void setInterval(Interval interval) {
		this.interval = interval;
	}

    public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

}
