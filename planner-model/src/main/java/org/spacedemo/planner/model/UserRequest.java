package org.spacedemo.planner.model;

import java.util.Objects;

public class UserRequest {

	private String id;

	private Position position;

	private String creationDate;

	private String emailUser;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getEmailUser() {
		return emailUser;
	}

	public void setEmailUser(String emailUser) {
		this.emailUser = emailUser;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, position, creationDate, emailUser);
	}

}
