package org.spacedemo.planner.satellite;

import org.spacedemo.planner.controller.PlanController;
import org.spacedemo.planner.model.Plan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping({ "/plan" })
public class PlanService {

	@Autowired
	private PlanController planController;

	@PostMapping(value = "/plan")
	public void get() {
		planController.compute();
	}

	@GetMapping(value = "/plan")
	public Plan post() {
		return planController.get();
	}

}
