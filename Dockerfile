FROM openjdk:8-jre-slim

COPY planner-app/target/planner-app.jar .

CMD ["java", "-jar", "planner-app.jar"]
