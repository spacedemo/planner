package org.spacedemo.planner.controller;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.spacedemo.planner.model.Acquisitions;
import org.spacedemo.planner.model.Plan;
import org.spacedemo.planner.model.UserRequest;
import org.spacedemo.planner.model.Interval;
import org.spacedemo.planner.model.Acquisition;
import org.spacedemo.planner.model.AccessParameters;
import org.spacedemo.planner.model.Access;
import org.spacedemo.planner.model.Accesses;
import org.spacedemo.planner.repository.AcquisitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

@Controller
public class PlanController {

	@Autowired
	private AcquisitionRepository aquisitionRepository;
	
	@Value("user-request-url")
	private String userRequestURL;

	@Value("guidance-url")
	private String guidanceURL;


	public void compute() {
	    final String userRequestUri = userRequestURL + "user_request";
	    
	    RestTemplate restTemplate = new RestTemplate();	    
	    class WrapperWithModel extends ArrayList<UserRequest>{};
	    List<UserRequest> response = restTemplate.getForObject(userRequestUri, WrapperWithModel.class);
	    
	    //Guidance url
		String guidanceUri = guidanceURL + "/guidance";

	    //For each request
		for(UserRequest request : response)
		{
			//Check if request not already planned
			if(aquisitionRepository.findById(request.getId()) != null)
			{
				AccessParameters parameters = new AccessParameters();
				parameters.setPosition(request.getPosition());
				
				Interval interval = new Interval(Instant.now(),Instant.now().plus(24, ChronoUnit.HOURS));
				parameters.setInterval(interval);

				//Compute access.
				Accesses accesses = restTemplate.postForObject(guidanceUri,parameters, Accesses.class);

				//Insert access
				if(accesses !=null && !accesses.getAccesses().isEmpty())
				{

					//TODO ? : check conflicts ?

					//Insert always first 
					Access access = accesses.getAccesses().get(0);

					//Insert always at beginning of access
					Acquisition acquisition = new Acquisition();
					acquisition.setId(request.getId());
					acquisition.setDate(access.getInterval().getBegin());

					//Store in repository
					aquisitionRepository.save(acquisition);
				}
			}
		}
	    
	    
	}

	public Plan get() {
		Plan newPlan = new Plan();
		
		newPlan.setAcquisitions(new Acquisitions());
		newPlan.getAcquisitions().getAcquisition().addAll(aquisitionRepository.findByStartGreaterThanDate(Instant.now().plus(1, ChronoUnit.HOURS)));
		return newPlan;
	}

}
