package org.spacedemo.planner.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Acquisitions {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToMany
	private List<Acquisition> acquisition;

	public List<Acquisition> getAcquisition() {
		return acquisition;
	}

	public void setAcquisition(List<Acquisition> acquisition) {
		this.acquisition = acquisition;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
