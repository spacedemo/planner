package org.spacedemo.planner.model;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Acquisition {


	@Id
	private String id;

	private Instant date;

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

}
