package org.spacedemo.planner.model;

import java.time.Instant;

public class Plan {

	private Instant creationDate;

	private Acquisitions acquisitions;

	public Acquisitions getAcquisitions() {
		return acquisitions;
	}

	public void setAcquisitions(Acquisitions acquisitions) {
		this.acquisitions = acquisitions;
	}

	public Instant getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Instant creationDate) {
		this.creationDate = creationDate;
	}

}
