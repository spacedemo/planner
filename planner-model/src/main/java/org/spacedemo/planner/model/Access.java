package org.spacedemo.planner.model;

public class Access {

	private Interval interval;
	
	private Position position;

	public Access(Position position, Interval interval) {
		this.position = position;
		this.interval = interval;
	}

	public Interval getInterval() {
		return interval;
	}
	
	public Position getPosition() {
		return position;
	}

	public void setInterval(Interval interval) {
		this.interval = interval;
	}
	
	public void setPosition(Position position) {
		this.position = position;
	}
}
